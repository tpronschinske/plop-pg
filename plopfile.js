var fs = require('fs');

module.exports = function (plop) {

    fs.readFile('questions/index.js', 'utf-8', (err, data) => {
        if (err) throw err;

        console.log(data.questionImports)
        
        var newValue = `let x = 0` 
        fs.writeFile('questions/index.js', newValue, 'utf-8', (err, data) => {
            if (err) throw err;
            console.log('Done!');
        })
    });

    // controller generator
    plop.setGenerator('question', {
        description: 'application controller logic',
        prompts: [{
            type: 'input',
            name: 'name',
            message: 'question name'
        }],
        actions: [{
            type: 'add',
            path: 'src/{{name}}.js',
            templateFile: 'plop-templates/question.hbs'
        }]
    });
};